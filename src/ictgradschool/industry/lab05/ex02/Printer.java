package ictgradschool.industry.lab05.ex02;

import javax.swing.text.Document;

public interface Printer {
    public void printDocument(Document d);

    public int getEstimateMinutesRemaining();

    public Error getError();
}
