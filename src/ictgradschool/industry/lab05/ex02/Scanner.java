package ictgradschool.industry.lab05.ex02;

import javax.swing.text.Document;

public interface Scanner {
    public Document getDocumnent();
    public boolean jobsDone();
    public Error getError();
}
