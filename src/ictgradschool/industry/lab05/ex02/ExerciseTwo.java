package ictgradschool.industry.lab05.ex02;

import javafx.animation.Animation;

/**
 * Main program for Exercise Two.
 */
public class ExerciseTwo {

    public void start() {

        IAnimal[] animals = new IAnimal[3];

        // TODO Populate the animals array with a Bird, a Dog and a Horse.
        animals[0] = new Bird();
        animals[1] = new Dog();
        animals[2] = new Horse();

        processAnimalDetails(animals);

    }

    private void processAnimalDetails(IAnimal[] list) {
        // TODO Loop through all the animals in the given list, and print their details as shown in the lab handout.
        // TODO If the animal also implements IFamous, print out that corresponding info too.
        for (int i = 0; i< list.length; i++){
            System.out.println(list[i].sayHello());
            if (list[i].isMammal()){
                System.out.println(list[i].myName() + " is a mammal");
            }
            else {
                System.out.println(list[i].myName() + " is a non-mammal");
            }
            System.out.println("Did I forget to tell you I have " + list[i].legCount() + " legs.");
            if (list[i] instanceof IFamous){
                IFamous obj = (IFamous)list[i];
                System.out.println("this is a famous name of my animal type: " + obj.famous());
            }

            System.out.println("-------------------------------------------------");
        }
    }

    public static void main(String[] args) {
        new ExerciseTwo().start();
    }
}
