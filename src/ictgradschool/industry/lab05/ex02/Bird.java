package ictgradschool.industry.lab05.ex02;

import ictgradschool.Keyboard;
import sun.awt.windows.ThemeReader;

/**
 * Represents a Bird.
 *
 * TODO Correctly implement these methods, as instructed in the lab handout.
 */
public class Bird implements IAnimal {

    private String name = Keyboard.readInput();

    @Override
    public String sayHello() {

        return myName() + " says tweet";

    }

    @Override
    public boolean isMammal() { return false; }

    @Override
    public String myName() {

        return name + " the bird";
    }

    @Override
    public int legCount() {
        return 2;
    }
}
