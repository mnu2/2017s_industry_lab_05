package ictgradschool.industry.lab05.ex02;

import ictgradschool.Keyboard;

/**
 * Represents a horse.
 *
 * TODO Make this implement IAnimal and IFamous, and provide appropriate implementations of those methods.
 */
public class Horse implements IAnimal, IFamous{

    private String name = Keyboard.readInput();

    @Override
    public String sayHello() {
        return myName() + " says neigh";
    }

    @Override
    public boolean isMammal() {
        return true;
    }

    @Override
    public String myName() {
        return name + " is a horse";
    }

    @Override
    public int legCount() {
        return 4;
    }

    @Override
    public String famous() {
        return "This is a famous name of my animal type: PharLap";
    }
}
