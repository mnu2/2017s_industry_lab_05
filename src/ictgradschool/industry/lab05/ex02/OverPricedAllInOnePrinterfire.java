package ictgradschool.industry.lab05.ex02;

import javax.swing.text.Document;

public class OverPricedAllInOnePrinterfire implements Printer, Scanner {

    @Override
    public void printDocument(Document d) { }

    @Override
    public int getEstimateMinutesRemaining() {
        return 0;
    }

    @Override
    public Error getError() {
        return null;
    }

    @Override
    public boolean jobsDone() {
        return false;
    }

    @Override
    public Document getDocumnent() {
        return null;
    }
}
