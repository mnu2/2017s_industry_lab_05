package ictgradschool.industry.lab05.ex02;

import javax.swing.text.Document;

public class PrintOmatic implements Printer {

    @Override
    public void printDocument(Document d) { }

    @Override
    public int getEstimateMinutesRemaining() {
        return 0;
    }

    @Override
    public Error getError() {
        return null;
    }

    public void getJobsDone(){}
}
