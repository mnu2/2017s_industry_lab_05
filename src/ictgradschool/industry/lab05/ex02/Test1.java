package ictgradschool.industry.lab05.ex02;

import sun.awt.windows.ThemeReader;

import java.rmi.activation.ActivationGroup_Stub;
import java.time.Year;
import java.util.logging.XMLFormatter;

public class Test1 extends SuperClass {

    public int x2 = 20;
    static int y2 = 20;

    Test1() {
        x2 = y2++;
    }

    public int foo2() {
        return y++;
    }

    public static int goo2() {
        return y2;
    }

    public static void main(String[] args) {
        SuperClass s2 = new Test1();
        Test1 obj = (Test1) s2;
        System.out.println("\nThe static Binding");
        System.out.println("S2.x = " + s2.x);
        System.out.println("S2.foo() = " + obj.foo2());
        System.out.println("S2.y = " + s2.y);

        System.out.println("S2.goo2() = " + obj.goo2());

    }
}
