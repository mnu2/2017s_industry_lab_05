package ictgradschool.industry.lab05.ex02;

import ictgradschool.Keyboard;

import java.security.Key;

/**
 * Represents a dog.
 *
 * TODO Make this class implement the IAnimal interface, then implement all its methods.
 */
public class Dog implements IAnimal{

   private String name = Keyboard.readInput();

    @Override
    public String sayHello() {
        return myName() + " says woof";
    }

    @Override
    public boolean isMammal() {
        return true;
    }

    @Override
    public String myName() {

        return name + " the dog";
    }

    @Override
    public int legCount() {
        return 4;
    }
}
